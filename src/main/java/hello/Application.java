package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class Application {



	@RequestMapping("/")
	public String home() {

		// Call service 2
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response
				= restTemplate.getForEntity("http://service2:8081/service2", String.class);

		String body = response.getBody();

		System.out.println("######### body = " + body);

		return "Hello Docker World from Service 1";
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
